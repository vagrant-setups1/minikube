# vagrant-minikube-setup


This installs the latest stable <code>Docker</code>, <code>Minikube</code>, <code>kubectl</code>, <code>helm</code>, <code>k9s</code> and <code>kubeless</code> versions into a <code>ubuntu/xenial64</code> VM.<br/>

Optional (commented lines): The <code>Kubernetes dashboard</code> is also installed and the port forwarded to the host machine, reachable [here](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/error?namespace=default)<br/>
The token for login into the <code>Kubernetes dashboard</code> is printed out as the last installation step.
