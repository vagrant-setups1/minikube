#!/bin/bash
echo "**** Begin installing kubectl"

#Install kubectl binary
sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

#Check the kubectl configuration
kubectl cluster-info

#Make kubectl work for your non-root user named vagrant
mkdir -p /home/vagrant/.kube
sudo cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
sudo chown -R vagrant:vagrant /home/vagrant/.kube

echo "**** End installing kubectl"

echo "**** Begin installing helm"

export HELM_VERSION=v3.0.2
wget https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz
tar -xzvf helm-$HELM_VERSION-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
sudo rm helm-$HELM_VERSION-linux-amd64.tar.gz
sudo rm -r linux-amd64/

echo "**** End installing helm"

echo "**** Begin installing kubeless"

export KUBELESS_VERSION=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
wget https://github.com/kubeless/kubeless/releases/download/$KUBELESS_VERSION/kubeless_linux-amd64.zip &&$
  unzip kubeless_linux-amd64.zip && \
  sudo mv bundles/kubeless_linux-amd64/kubeless /usr/local/bin/kubeless
sudo rm kubeless_linux-amd64.zip
sudo rm -r bundles/

echo "**** End installing kubeless"

echo "**** Begin installing k9s"

export K9S_VERSION=0.12.0
wget https://github.com/derailed/k9s/releases/download/$K9S_VERSION/k9s_${K9S_VERSION}_Linux_x86_64.tar.gz
mkdir /tmp/k9s
tar -xzvf k9s_${K9S_VERSION}_Linux_x86_64.tar.gz -C /tmp/k9s/
sudo mv /tmp/k9s/k9s /usr/local/bin/k9s
sudo rm k9s_${K9S_VERSION}_Linux_x86_64.tar.gz
sudo rm -r /tmp/k9s/

echo "**** End installing k9s"

#echo "**** Begin preparing dashboard"

#kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta4/aio/deploy/recommended.yaml
#kubectl proxy --address='0.0.0.0'

#echo "**** End preparing dashboard"

#echo "**** Show deployment controller token"

#kubectl -n kube-system describe secret deployment-controller-token
