# install k9s
export K9S_VERSION=0.12.0
wget https://github.com/derailed/k9s/releases/download/$K9S_VERSION/k9s_${K9S_VERSION}_Linux_x86_64.tar.gz
mkdir /tmp/k9s
tar -xzvf k9s_${K9S_VERSION}_Linux_x86_64.tar.gz -C /tmp/k9s/
sudo mv /tmp/k9s/k9s /usr/local/bin/k9s
sudo rm k9s_${K9S_VERSION}_Linux_x86_64.tar.gz
sudo rm -r /tmp/k9s/

